

abstract class Animal {
  final String animalType;
  final String animalName;
  final int age;
  double energyLevel = 1.0;
  double hungryLevel = 0.5;
  double happyLevel = 1.0;
  bool sleepState = false;
  String hobbies;

  Animal(
      {required this.animalType,
      required this.animalName,
      required this.age,
      required this.hobbies});

  void eat(food) {
    if (food == 0) {
      print("У тебя нет корма для животных (");
    }
    else if (hungryLevel >= 0 && hungryLevel <= 1) {
      while (food >=2 && food <=5) {
        food--;
        print("Потрачено: Корм для животных x1, осталось еще $food штуки");
        break;
      }
      print('$animalType $animalName кушает, подожди немного ...');
      Future.delayed(Duration(seconds: 2), () {
        print("$animalType $animalName покушал, что будем делать дальше?");
      });
      hungryLevel = hungryLevel + 0.25;
    } else if (hungryLevel >= 1) {
      print(
          "$animalType $animalName уже наелся и пока не голоден, поиграй с ним :)");
      play(food);
    }
  }

  void sleep() {
    if (sleepState = false) {
      print('$animalType $animalName решил немного отдохнуть, подожди немного ...');
      sleepState = true;
      Future.delayed(const Duration(seconds: 2), () {
        print("$animalType $animalName выспался, что будем делать дальше?");
      });


    } else if (sleepState = true) {
      print('$animalType $animalName наелся и спит, подожди немного ...');
      sleepState = false;
      Future.delayed(const Duration(seconds: 2), () {
        print("$animalType $animalName проснулся, что будем делать дальше?");
      });
    }
  }

  void play(food) {
    if (hungryLevel <= 0.25) {
      print(
          "$animalType $animalName сильно голоден и играть с ним опасно, сначала хорошенько накорми его!");
      eat(food);
    } else if (hungryLevel > 0.25) {
      print("$animalType $animalName играет с тобой ...");
      energyLevel = energyLevel - 0.25;
      Future.delayed(const Duration(seconds: 3), () {
        print("$animalType $animalName наигрался, что будем делать дальше?");
      });
    }
    else if (energyLevel <= 0.25) {
      print("$animalType $animalName сильно устал и играть с ним опасно, сначала ему нужно выспаться!");
      sleep();
    }
  }

  void showInfo() {
    print(
        "Это $animalType. Его зовут $animalName и ему уже $age лет. $hobbies");
    print("");
  }

  @override
  String toString() {
    return '$runtimeType $age';
  }
}

class Fish extends Animal {
  Fish(
      {required String animalType,
      required String animalName,
      required int age,
      required String hobbies})
      : super(
            animalName: animalName,
            animalType: animalType,
            age: age,
            hobbies: hobbies);
}

class Mammal extends Animal {
  Mammal(
      {required String animalType,
      required String animalName,
      required int age,
      required String hobbies})
      : super(
            animalName: animalName,
            animalType: animalType,
            age: age,
            hobbies: hobbies);
}

class Bird extends Animal {
  Bird(
      {required String animalType,
      required String animalName,
      required int age,
      required String hobbies})
      : super(
            animalName: animalName,
            animalType: animalType,
            age: age,
            hobbies: hobbies);
}

class Shark extends Fish {
  Shark(
      {required String animalType,
      required String animalName,
      required int age,
      required int maxHp,
      required String hobbies})
      : super(
            animalName: animalName,
            animalType: animalType,
            age: age,
            hobbies: hobbies);
}

class Bear extends Mammal {
  Bear(
      {required String animalType,
      required String animalName,
      required int age,
      required String hobbies})
      : super(
            animalName: animalName,
            animalType: animalType,
            age: age,
            hobbies: hobbies);

  @override
  void sleep() {
    if (sleepState = false) {
      print('$animalName решил немного отдохнуть, подожди немного ...');
        print("О-оу, мишка $animalName сладко спал, а теперь он проснулся");
    } else if (sleepState = true) {
      print('$animalName наелся и спит, подожди немного');
      print("$animalName проснулся, теперь ты можешь поиграть с ним");
    }
  }
}

class Tiger extends Mammal {
  Tiger({required String animalType,
    required String animalName,
    required int age,
    required String hobbies})
      : super(
      animalName: animalName,
      animalType: animalType,
      age: age,
      hobbies: hobbies);
}


class Gorilla extends Mammal {
  Gorilla(
      {required String animalType,
      required String animalName,
      required int age,
      required String hobbies})
      : super(
            animalName: animalName,
            animalType: animalType,
            age: age,
            hobbies: hobbies);
}

class Colibri extends Bird {
  Colibri(
      {required String animalType,
      required String animalName,
      required int age,
      required String hobbies})
      : super(
            animalName: animalName,
            animalType: animalType,
            age: age,
            hobbies: hobbies);
}
