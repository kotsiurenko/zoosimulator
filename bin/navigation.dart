import 'dart:io';
import 'animal_operations.dart';
import 'quiz.dart';
import 'sketch.dart';

void mainMenu(balance, food) {
  while (true) {
    zooSketch();
    print("Привет, рад что ты зашел к нам в зоопарк!");
    print("Если хочешь зайти внутрь - купи билет, он стоит 10\$");
    print("");
    print("1) Купить билет");
    print("2) Выйти");

    String mainMenuOperation = stdin.readLineSync()!;
    switch (mainMenuOperation) {
      case "1":
        if (balance == 0) {
          print("У тебя не хватает денег на билет :(");
          print("Но ты можешь ответить на пару вопросов и заработать на билет");
          quizMenu(balance, food);
        } else if (balance >= 10) {
          balance = balance - 10;
          print("Получено: Входной билет");
          print(
              "У тебя осталось $balance\$, этого как раз хватит на вкусняшки для животных. Хочешь купить их? y/n");
          String buyAnimalFood = stdin.readLineSync()!;
          switch (buyAnimalFood) {
            case "y":
              balance = balance - 5;
              food = food + 5;
              print("Получено: Корм для животных x$food");
              animalPickMenu(food);
              break;
            case "n":
              print("Но ведь ты не сможешь покормить зверят!");
              food = 0;
              animalPickMenu(food);
          }
        }
        break;
      case "2":
        print("Пока :(");
        return;
      default:
        print("Покупай билет или уходи!");
        return;
    }
  }
}

void animalPickMenu(food) {
  print("");
  print(
      "⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻");
  zooMapSketch();
  String animalPick = stdin.readLineSync()!;
  switch (animalPick) {
    case "1":
      print(
          "⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻");
      print("");
      sharkSketch();
      sharkOperations(food);
      break;

    case "2":
      print(
          "⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻");
      print("");
      tigerSketch();
      tigerOperations(food);
      break;
    case "3":
      print(
          "⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻");
      print("");
      bearSketch();
      bearOperations(food);
      break;

    case "4":
      print(
          "⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻");
      print("");
      gorillaSketch();
      gorillaOperations(food);
      break;

    case "5":
      print(
          "⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻");
      print("");
      colibriSketch();
      colibriOperations(food);
      break;
  }
}

void animalOperationMenu() {
  print("");
  print("Выбери что бы ты хотел сделать с животным:");
  print("1) - покормить");
  print("2) - поиграть");
  print("3) - уложить спать");
  print("4) - выйти назад");
}
