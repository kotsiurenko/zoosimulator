void zooSketch () {
  print("");
  print("                 --- ДОБРО ПОЖАЛОВАТЬ В ЗООПАРК! ---           ");
  print("");
  print('                         ___________________                    ');
  print('                      ____|______ZOO______|____                 ');
  print('     .      .      .      |     _____     |      .      .      .');
  print('     |------|------|------|_____|___|_____|------|------|------|');
  print("⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻");
}

void zooMapSketch () {
  print("");
  print("           --- К КАКОМУ ЖИВОТНОМУ ТЫ ЗАЙДЕШЬ В ГОСТИ? ---       ");
  print("");
  print("       ₁            ₂            ₃           ₄            ₅    ");
  print("   |   🦈   |   |   🐅   |   |   🐻   |   |   🦍   |   |   🐦   |");
  print("⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻");
}


void sharkSketch () {
  print("    --- ВОЛЬЕР АКУЛЫ ---       ");
  print("");
  print("  |           🦈          |   ");
  print(" ⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻");
  print("");
}

void sharkEatSketch () {
  print("");
  print("  |        🐠 🦈          |   ");
  print(" ⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻");
  print("");
}

void sharkPlaySketch () {
  print("");
  print("  |        ⚽ 🦈          |   ");
  print(" ⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻");
  print("");
}

void sharkSleepSketch() {
  print("");
  print("  |         💤🦈          |   ");
  print(" ⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻");
  print("");
}

void tigerSketch() {
  print("    --- ВОЛЬЕР ТИГРА ---       ");
  print("");
  print("  |          🐅           |   ");
  print(" ⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻");
  print("");
}

void tigerEatSketch () {
  print("");
  print("  |        🍖 🐅          |   ");
  print(" ⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻");
  print("");
}

void tigerPlaySketch () {
  print("");
  print("  |        ⚽ 🐅          |   ");
  print(" ⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻");
  print("");
}

void tigerSleepSketch() {
  print("");
  print("  |         💤🐅          |   ");
  print(" ⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻");
  print("");
}


void bearSketch() {
  print("   --- ВОЛЬЕР МЕДВЕДЯ ---       ");
  print("");
  print("  |          🐻           |   ");
  print(" ⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻");
  print("");
}

void bearEatSketch() {
  print("");
  print("  |        🍯 🐻          |   ");
  print(" ⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻");
  print("");
}

void bearPlaySketch() {
  print("");
  print("  |        ⚽ 🐻          |   ");
  print(" ⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻");
  print("");
}

void bearSleepSketch() {
  print("");
  print("  |         💤🐻          |   ");
  print(" ⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻");
  print("");
}


void gorillaSketch() {
  print("   --- ВОЛЬЕР ГОРИЛЛЫ ---       ");
  print("");
  print("  |           🦍          |   ");
  print(" ⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻");
  print("");
}

void gorillaEatSketch() {
  print("");
  print("  |        🍌 🦍          |   ");
  print(" ⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻");
  print("");
}

void gorillaPlaySketch() {
  print("");
  print("  |        ⚽ 🦍          |   ");
  print(" ⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻");
  print("");
}

void gorillaSleepSketch() {
  print("");
  print("  |         💤🦍          |   ");
  print(" ⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻");
  print("");
}


void colibriSketch() {
  print("   --- ВОЛЬЕР КОЛИБРИ ---       ");
  print("");
  print("  |          🐦          |   ");
  print(" ⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻");
  print("");
}

void colibriEatSketch() {
  print("");
  print("  |         🌺 🐦          |   ");
  print(" ⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻");
  print("");
}

void colibriPlaySketch() {
  print("");
  print("  |         ⚽ 🐦          |   ");
  print(" ⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻");
  print("");
}

void colibriSleepSketch() {
  print("");
  print("  |         💤🐦          |   ");
  print(" ⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻");
  print("");
}


