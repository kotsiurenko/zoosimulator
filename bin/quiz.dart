import 'dart:io';

import 'navigation.dart';

void quizGame(balance, food) {
  quizQuestion1(balance, food);
  quizQuestion2(balance, food);
  quizQuestion3(balance, food);
}

void quizMenu(balance, food) {
  print("Ты готов сыграть в игру? y/n");
  String quizStartOperation = stdin.readLineSync()!;
  switch (quizStartOperation) {
    case "y":
      print("");
      print(
          "⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻");
      print("Тогда начнем!");
      quizGame(balance, food);
      break;
    case "n":
      print("Пока :(");
      return;
    default:
      print("Выбери y или n!");
      return;
  }
}

void quizQuestion1(balance, food) {
  print("");
  print("Отгадай первую загадку:");
  print("В рот этой рыбе палец не клади,");
  print("Смотри, за борт ты к ней не упади,");
  print("Ведь эта хищница в один присест");
  print("Тебя возьмет и просто съест.");
  print("");
  print("1 - Акула");
  print("2 - Крокодил");
  print("3 - Дикая креветка");
  print("4 - Морская свинка");
  String question1 = stdin.readLineSync()!;
  switch (question1) {
    case "1":
      print("Это правильный ответ");
      balance = balance + 5;
      print("Ты заработал $balance\$, осталось еще немного");
      quizQuestion2(balance, food);
      break;
    case "2":
      print("Упс, это не правильный ответ! Попробуй еще раз");
      quizMenu(balance, food);
      break;
    case "3":
      print("Упс, это не правильный ответ! Попробуй еще раз");
      quizMenu(balance, food);
      break;
    case "4":
      print("Упс, это не правильный ответ! Попробуй еще раз");
      quizMenu(balance, food);
      break;
    default:
      print("Ошибка! Выбери ответ");
  }
}

void quizQuestion2(balance, food) {
  print("");
  print(
      "⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻");
  print("Отгадай вторую загадку:");
  print("Кот размерами с быка,");
  print("Полосатые бока,");
  print("Этот хитрый умник");
  print("Обитает в джунглях!");
  print("");
  print("1 - Зебра");
  print("2 - Тигр");
  print("3 - Бык размером с кота");
  print("4 - Леопард");
  String question2 = stdin.readLineSync()!;
  switch (question2) {
    case "1":
      print("Упс, это не правильный ответ! Попробуй еще раз");
      quizMenu(balance, food);
      break;
    case "2":
      print("Это правильный ответ");
      balance = balance + 5;
      print(
          "Ты заработал $balance\$, на билет уже хватает, но лучше сделать запас");
      quizQuestion3(balance, food);
      break;
    case "3":
      print("Упс, это не правильный ответ! Попробуй еще раз");
      quizMenu(balance, food);
      break;
    case "4":
      print("Упс, это не правильный ответ! Попробуй еще раз");
      quizMenu(balance, food);
      break;
    default:
      print("Ошибка! Выбери ответ");
  }
}

void quizQuestion3(balance, food) {
  print("");
  print(
      "⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻");
  print("Отгадай последнюю загадку:");
  print("Ой, смотрите, человек!");
  print("А на теле - чёрный мех.");
  print("Мы ошиблись, смотрит мило");
  print("Из кустов на нас...");
  print("");
  print("1 - Саня");
  print("2 - Лев");
  print("3 - Горилла");
  print("4 - Волк");
  String question2 = stdin.readLineSync()!;
  switch (question2) {
    case "1":
      print("Это правильный ответ");
      balance = balance + 5;
      print(
          "Ты заработал $balance\$, теперь тебе хватает и на билет, и на кое-что еще :) Заходи в зоопарк!");
      mainMenu(balance, food);
      break;
    case "2":
      print("Упс, это не правильный ответ! Попробуй еще раз");
      quizMenu(balance, food);
      break;
    case "3":
      print("Это правильный ответ");
      balance = balance + 5;
      print(
          "Ты заработал $balance\$, теперь тебе хватает и на билет, и на кое-что еще :) Заходи в зоопарк!");
      mainMenu(balance, food);
      break;
    case "4":
      print("Упс, это не правильный ответ! Попробуй еще раз");
      quizMenu(balance, food);
      break;
    default:
      print("Ошибка! Выбери ответ");
  }
}