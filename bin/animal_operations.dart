
import 'dart:io';

import 'animals.dart';
import 'navigation.dart';
import 'sketch.dart';

void sharkOperations(food) {
  Shark shark =
  Shark(animalType: "акула", animalName: "Альфред", age: 5, maxHp: 1, hobbies: "У этих рыб, кроме зубов, не ни одной косточки, только хрящи. \nАкула должна все время двигаться, чтобы дышать, если она остановиться, то может задохнуться и утонуть.");
  shark.showInfo();
  while (true) {
    animalOperationMenu();
    String sharkPickOperations = stdin.readLineSync()!;
    switch (sharkPickOperations) {
      case "1":
        sharkEatSketch();
        shark.eat(food);
        break;
      case "2":
        sharkPlaySketch();
        shark.play(food);
        break;
      case "3":
        sharkSleepSketch();
        shark.sleep();
        break;
      case "4":
        animalPickMenu(food);
        break;
    }
  }
}

void tigerOperations(food) {
  Tiger tiger =
  Tiger(animalType: "тигр", animalName: "Виталий", age: 8, hobbies: "Тигр – один из самых крупных представителей хищного царства, длина его тела – от полутора до трёх метров. \nЦвет меха тигра — красновато-желтый с красивыми черными полосами по всему телу. Встречаются в природе и тигры-стиляги: белого или чёрного цвета. Тигр – кошка с особой грацией.");
  tiger.showInfo();
  while (true) {
    animalOperationMenu();
    String tigerPickOperations = stdin.readLineSync()!;
    switch (tigerPickOperations) {
      case "1":
        tigerEatSketch();
        tiger.eat(food);
        break;
      case "2":
        tigerPlaySketch();
        tiger.play(food);
        break;
      case "3":
        tigerSleepSketch();
        tiger.sleep();
        break;
      case "4":
        animalPickMenu(food);
        break;
    }
  }
}

void bearOperations(food) {
  Bear bear = Bear(animalType: "медведь", animalName: "Тэд", age: 10,hobbies: "Медведи всеядны, хорошо лазают и плавают, быстро бегают, могут стоять и проходить короткие расстояния на задних лапах.");
  bear.showInfo();
  while (true) {
    animalOperationMenu();
    String bearPickOperations = stdin.readLineSync()!;
    switch (bearPickOperations) {
      case "1":
        bearEatSketch();
        bear.eat(food);
        break;
      case "2":
        bearPlaySketch();
        bear.play(food);
        break;
      case "3":
        bearSleepSketch();
        bear.sleep();
        break;
      case "4":
        animalPickMenu(food);
        break;
    }
  }
}

void gorillaOperations(food) {
  Gorilla gorilla =
  Gorilla(animalType: "горилла", animalName: "Чич", age: 12, hobbies: "Гориллы — крупнейшие человекообразные обезьяны и приматы вообще. \nНаряду с шимпанзе и орангутаном они наиболее близки к человеку");
  gorilla.showInfo();
  while (true) {
    animalOperationMenu();
    String gorillaPickOperations = stdin.readLineSync()!;
    switch (gorillaPickOperations) {
      case "1":
        gorillaEatSketch();
        gorilla.eat(food);
        break;
      case "2":
        gorillaPlaySketch();
        gorilla.play(food);
        break;
      case "3":
        gorillaSleepSketch();
        gorilla.sleep();
        break;
      case "4":
        animalPickMenu(food);
        break;
    }
  }
}

void colibriOperations(food) {
  Colibri colibri =
  Colibri(animalType: "колибри", animalName: "Байрактар", age: 1, hobbies: "Колибри летает не как все птицы. \nВ отличие от остальных пернатых малышка колибри может летать, как головой, так и хвостом вперёд. \nЕй не составит особого труда зависнуть в пространстве на одном месте, взлететь практически вертикально, подобно вертолету, и также отвесно упасть вниз.");
  colibri.showInfo();
  while (true) {
    animalOperationMenu();
    String colibriPickOperations = stdin.readLineSync()!;
    switch (colibriPickOperations) {
      case "1":
        colibriEatSketch();
        colibri.eat(food);
        break;
      case "2":
        colibriPlaySketch();
        colibri.play(food);
        break;
      case "3":
        colibriSleepSketch();
        colibri.sleep();
        break;
      case "4":
        animalPickMenu(food);
        break;
    }
  }
}